import Hello from "../components/hello";

const IndexPage = () => <Hello name="World" />;

export default IndexPage;
