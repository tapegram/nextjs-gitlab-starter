type Props = {
  name: string;
};

const Hello = (props: Props) => <p>Hello {props.name}!</p>;

export default Hello;
